package com.example.login2;

import static android.content.ContentValues.TAG;
import static androidx.core.content.FileProvider.getUriForFile;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.core.content.FileProvider;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class HelperMethods {

    public static File getFile(Context context) {
        File base_path = new File(context.getFilesDir(), "contacts");
        if (!base_path.exists())
            base_path.mkdir();

        File newFile = new File(base_path, "contacts.txt");
        try {
            newFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        return getUriForFile(context, "com.example.login2", newFile);
        return newFile;
    }

    public static void writeToFile(String data,Context context) {
        File file = getFile(context);
        saveContents(data, file);
    }

    public static void saveContents(String contents, File file) {
        try {
            Log.d("EK", "saveContents: "+file.getPath());
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(contents);
            bw.close();
        } catch (IOException e) {
            Log.d("EK", "saveContents: "+e.getMessage());
            e.printStackTrace();
        }
    }
}
