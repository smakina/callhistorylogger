package com.example.login2.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "tbl_cal_log")
public class MCallLog {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String phone;
    public String type;
    public String date;
    public String duration;

    public MCallLog(String phone, String type, String date, String duration) {
        this.phone = phone;
        this.type = type;
        this.date = date;
        this.duration = duration;
    }
}
