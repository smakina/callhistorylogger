package com.example.login2.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.login2.model.MCallLog;
import com.example.login2.repo.CallLogRepo;

import java.util.List;

public class CallLogViewModel extends AndroidViewModel {
    private CallLogRepo callLogRepo;

    public CallLogViewModel(@NonNull Application application) {
        super(application);
        callLogRepo = new CallLogRepo(application);
    }

    public void insert(MCallLog... MCallLogs) {
        callLogRepo.insert(MCallLogs);
    }

    public LiveData<List<MCallLog>> getCallLogList() {
        return callLogRepo.getCallLog();
    }

}
