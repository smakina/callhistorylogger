package com.example.login2;

import android.content.Context;

public interface RepoInterface<T> {
    default void onStart(){};
    default void onSuccess(T t){};
    default void onComplete(){};
    default void onError(Context context, Throwable e) {};
}
