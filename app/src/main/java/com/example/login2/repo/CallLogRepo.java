package com.example.login2.repo;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.login2.db.LocalDataBase;
import com.example.login2.db.dao.CallLogDao;
import com.example.login2.model.MCallLog;
import com.example.login2.retrofit.ApiInterface;
import com.example.login2.retrofit.RetrofitUtility;

import java.util.List;

public class CallLogRepo {
    private CallLogDao callLogDao;
    private ApiInterface apiInterface;

    public CallLogRepo(Application application) {
        callLogDao = LocalDataBase.getRoomDatabase(application).userDao();
        apiInterface = RetrofitUtility.getRetrofitClient(application).create(ApiInterface.class);
    }

    public LiveData<List<MCallLog>> getCallLog() {
        return callLogDao.getCallLog();
    }

    public void insert(MCallLog... MCallLogs) {
        LocalDataBase.databaseWriterService.execute(()->{
            callLogDao.insert(MCallLogs);
        });
    }
}
