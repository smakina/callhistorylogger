package com.example.login2.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.login2.model.MCallLog;

import java.util.List;

@Dao
public interface CallLogDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(MCallLog... MCallLog);

    @Query("SELECT * FROM tbl_cal_log")
    LiveData<List<MCallLog>> getCallLog();


}
