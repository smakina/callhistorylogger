package com.example.login2.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.login2.db.dao.CallLogDao;
import com.example.login2.model.MCallLog;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = { MCallLog.class },
        version = 1, exportSchema = false)
//@TypeConverters({Convertors.class})
public abstract class LocalDataBase extends RoomDatabase {
    public static int dbVersion = 1;

    public abstract CallLogDao userDao();

    private static volatile LocalDataBase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriterService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static LocalDataBase getRoomDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (RoomDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), LocalDataBase.class, "login_db"+dbVersion)
//                            .fallbackToDestructiveMigration()
                            .addCallback(roomDBCallBack).build();
                }
            }
        }
        return INSTANCE;
    }

    private final static Callback roomDBCallBack = new Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            databaseWriterService.execute(() -> {

            });
        }
    };
}
