package com.example.login2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog;

import com.example.login2.databinding.ActivityMainBinding;
import com.example.login2.model.MCallLog;
import com.example.login2.viewmodel.CallLogViewModel;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private CallLogViewModel callLogViewModel;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        callLogViewModel = new ViewModelProvider(this).get(CallLogViewModel.class);
        loadContacts();
    }

    private void loadContacts() {
        Dexter.withContext(this).withPermissions(Manifest.permission.READ_CALL_LOG,
                Manifest.permission.WRITE_EXTERNAL_STORAGE ,
                Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                if (multiplePermissionsReport.areAllPermissionsGranted()) {
                    List<MCallLog> mCallLogList = getCallDetails();
                    MCallLog[] mCallLogs = mCallLogList.toArray(new MCallLog[0]);
                    callLogViewModel.insert(mCallLogs);
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {

            }
        }).check();

    }

    private List<MCallLog> getCallDetails() {
        List<MCallLog> mCallLogList = new ArrayList<>();

        Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null,
                null, null, null);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

        StringBuilder sb = new StringBuilder();

        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            String callDuration = managedCursor.getString(duration);
            String dir = null;

            int dircode = Integer.parseInt(callType);

            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }

            sb.append(phNumber).append("\n");
            mCallLogList.add(new MCallLog(phNumber, dir, callDate, ""+callDuration ));
        }

        HelperMethods.writeToFile(sb.toString(), this);
        managedCursor.close();

        return mCallLogList;
    }

}