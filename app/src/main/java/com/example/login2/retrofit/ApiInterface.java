package com.example.login2.retrofit;


import com.example.login2.model.MCallLog;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("users")
    Call<MCallLog> getUser(@Query("username")  String username, @Query("password")  String password );

    @GET("users")
    Call<List<MCallLog>> getUsers();

    @FormUrlEncoded
    @POST("login")
    Call<MCallLog> saveUser(
            @Field("name") String userName,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("save")
    Call<ResponseBody> saveUser(
            @Field("fname") String fname,
            @Field("lname") String lname,
            @Field("uname") String uname,
            @Field("password") String password
    );

}