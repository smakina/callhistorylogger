package com.example.login2.retrofit;

import android.app.Application;
import android.content.Context;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitUtility extends Application {

    private static RetrofitUtility mInstance;
    private static Retrofit retrofit = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized RetrofitUtility getmInstance() {
        return mInstance;
    }

    public static Retrofit getRetrofitClient(Context context) {
        if (retrofit == null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

            httpClient.addInterceptor(chain -> {
                Request request = chain.request().newBuilder()
//                        .addHeader("x-rapidapi-host", "shazam.p.rapidapi.com")
//                        .addHeader("x-rapidapi-key", "b0236180dcmsh582bbebfab69fe9p1bb262jsnd94ffbdbaba2")
                        .build();
                return chain.proceed(request);
            });

            retrofit = new Retrofit.Builder()
                    .client(httpClient.build())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://locahost/").build();
        }

        return retrofit;
    }
}
